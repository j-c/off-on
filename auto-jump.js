// ==UserScript==
// @name         第三方链接自动跳转
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        **://**/**target=**
// @run-at document-start
// ==/UserScript==

(function() {
    'use strict';
    let targetKey = {
        default:'target',
        'xx.com':'url'
    }
    let host = location.hostname
    let search = location.search.match(/([^=\?&]+)=([^=\?&]+)/g)
    if(search){
            let targetParam = search.filter(item=>item.startsWith(targetKey[host]||targetKey.default))?.[0]
            let url = targetParam?.match(/[^=\?&]+=([^=\?&]+)/)?.[1]
            url = decodeURIComponent(url)
            location = url
    }
    // Your code here...
})();