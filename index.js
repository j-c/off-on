function run() {
    let btnSearch = false
    let timer = null
    let userClickTimer = null
    let doc = document
    let maxReceiveOrder = 10 // 最多同时拥有多少单子。
    let stop = false

    let iframe = window.top.document.querySelector('[src*="/dian/comment/index"]')
    if (iframe) {
        doc = iframe.contentDocument
        btnSearch = doc.getElementById('btnSearch')
    }

    if (btnSearch) {
        // 点击开始搜索
        console.log("点击【查询】按钮，开始自动每分钟抢单，刷新页面，关闭自动抢单，扣3分")

        // 自动更新积分
        clearInterval(timer)

        timer = setInterval(() => {
            //  关闭所有弹框
            Array.from(window.top.document.querySelectorAll('.lz-toast .lz-list .d-toast-close')).forEach(item => item.click())
            getScore().then(score => {
                let span = doc.querySelector('#loginOut > span')
                if (span) span.innerText = '积分：' + score
            })
        }, 5000)

        // 设置分页数据
        doc.querySelector('.pagination-page-list').value = 100
        // 开始查询
        btnSearch.addEventListener('click', () => {
            stop = false
            loopGet(1)
        }, false)
    }

    function loopGet(type) {
        clearTimeout(userClickTimer)
        let form = doc.getElementById('frmSearch')
        let data = formser(form)
        // 从页面直接处理第一页数据
        let list = readDataFromPage(data)
        console.log(list)
        return

        if (data.user_id) {
            postJson(list, 0)
            return
        }
        if (type === 1) {
            receiveList(list).then(res => {

            })
        } else {
            getList({
                page: opt.userClick ? 1 : page,
                company: company.value,
                opt
            }).then(resList => {
                let list = resList.list
                console.log(resList)
                receiveList(list)
            })

        }
    }

    function readDataFromPage(data) {
        // 难度 ，0 ，1，2。只抢0的。
        let difficulty = data.difficulty
        if (difficulty === '') {
            difficulty = '0'
        }

        // 从页面直接处理第一页数据
        let thead = doc.querySelectorAll('.datagrid-view2 .datagrid-body .datagrid-htable tr td')
        let tr = doc.querySelectorAll('.datagrid-view2 .datagrid-body .datagrid-btable tr')
        return Array.from(tr)
            .map(item => {
                let td = item.querySelectorAll('td')
                let subList = Array.from(td).map(i => i.innerText.trim())
                let subMap = {}
                subList.forEach((sub, index) => {
                    subMap[thead[index].innerText.trim()] = sub
                })
                return subMap
            })
        // .filter(item => new RegExp(`\\[(${difficulty})\\]`).test(item[10]) && /我来处理/.test(item[11]) && /商家余额不足/.test(item[7]))
    }

    function receiveList(list) {
        if (list.length > maxReceiveOrder) {
            console.log('找到：', list.length, '个单子，只需要抢到前:', maxReceiveOrder, '单')
            list.length = maxReceiveOrder
        }

        list = list.map(item => receive(item))
        return Promise.all(list).then(res => {
            res = res.filter(item => !!item)
            if (res.length === 0) {
                console.log('没有可抢的单子,一分钟后再次抢单')
            } else {
                let success = res.filter(i => i.status === 0)
                let faild = res.filter(i => i.status !== 0)
                postJson(list, 1)

                console.log('本次成功', success.length, '单')
                if (faild.length) {
                    console.log('失败', faild.length, '单')
                    console.log('出现失败单，暂时停止抢单，刷新后重新开始')
                    stop = true
                }
            }

            if (stop) {
                return
            }

            userClickTimer = setTimeout(() => {
                loopGet()
            }, 60 * 1000)
        })
    }

    //领取任务
    function receive(item) {
        return new Promise(resolve => {
            getJson(`${location.protocol}//${location.host}/dian/comment/receive`, {
                id: item.id
            }).then(res => {
                if (res.status === 0) {
                    console.log('抢单成功', item.order_id, item.company_name, item.product_name)
                } else {
                    console.log('抢单失败：', res.msg, item.order_id, item.company_name, item.product_name)
                }
                res.item = item
                resolve(res)
            })
        })
    }

    function getScore() {
        return getHtml(`${location.protocol}//${location.host}/index/index`).then(res => {
            let score = res.match(/积分：(\d+)/)[1]
            return score
        })
    }

//获取任务列表
    function getList(options) {
        return new Promise(resolve => {
            let data = {
                page: options.page,
                rows: pageSize,
            }
            if (options.opt.userClick) {
                data = {
                    ...data,
                    "comment_time_type": 1,
                    "order_id": '',
                    "product_id": '',
                    "post_tel": '',
                    "rank_type": '',
                    "user_id": '',
                    "receive_status": 2, // 按照采集时间排序
                    "is_user_id": '',
                    "difficulty_sign": "=",
                    "difficulty": '',
                    "client_status": '',
                    "company_id": options.company,
                    "orderby": 1,
                    ...formser(options.opt.form),
                    "start_date": new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000).toLocaleDateString().replace(/\//g, '-'),
                    "end_date": new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString().replace(/\//g, '-'),
                }
            } else {
                data = {
                    ...data,
                    "comment_time_type": 1,
                    "start_date": new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000).toLocaleDateString().replace(/\//g, '-'),
                    "end_date": new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString().replace(/\//g, '-'),
                    "order_id": '',
                    "product_id": '',
                    "post_tel": '',
                    "rank_type": '',
                    "user_id": '',
                    "receive_status": 2, // 按照采集时间排序
                    "is_user_id": '',
                    "difficulty_sign": "=",
                    "difficulty": '',
                    "client_status": '',
                    "company_id": options.company,
                    "orderby": 1
                }
            }
            console.log('查询条件', data)
            getJson(`${location.protocol}//${location.host}/dian/comment/get-list`, data).then(res => {
                if (res.status === 0) {
                    let list = res.rows.map(item => {
                        item.difficulty * 1
                        return item
                    })
                        .filter(item => item.user_id == 0 && item.status == 2 && item.surpass_30 == 0 && item.is_shield != 1)
                    list = list
                        .filter(item => item.post_tel !== '商家余额不足')
                        .filter(item => (data.difficulty == '' || data.difficulty === 2 && data.difficulty_sign === '<') ? (item.difficulty == 0 /* || item.difficulty == 1*/) : item.difficulty == data.difficulty)
                    res.list = list.sort((v1, v2) => {
                        return v1.difficulty > v2.difficulty ? 1 : -1
                    })
                    resolve(res)
                } else {
                    console.log(res)
                }
            })
        })
    }

    function getJson(url, data) {
        let dataStr = []

        for (let i in data) {
            dataStr.push(`${i}=${data[i]}`)
        }
        dataStr = dataStr.join('&')

        return new Promise(success => {
            GM_xmlhttpRequest({
                method: "post",
                url: url,
                data: dataStr,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                responseType: 'json',
                onload: function (res) {
                    if (res.status === 200) {
                        try {
                            if (typeof res.responseText === 'string') {
                                success(JSON.parse(res.responseText))
                            } else {
                                success(res.responseText)
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    } else {
                        console.log(data, res)
                    }
                },
                onerror: function (err) {
                    console.log(data, err)
                }
            });
        })
    }

    function postJson(list, type) {
        let all = []
        for (let i = 0; i < list.length; i++) {
            all.push(list[i])
            if (all.length === 10 || list.length - 1 === i) {
                getemail(all, type)
            }
        }
    }

    function getemail(list, type) {
        list = encodeURIComponent(JSON.stringify(list))
        let data = JSON.stringify({
            user_id: "user_ErabEuVeaUVqjaBC6mqqQ",
            service_id: "service_kv0eq59",
            template_id: "template_499uoo3",
            accessToken: '9e206240bc035ad698c1606cc721df6a',
            template_params: {
                title: 'emailjs ' + type,
                content: list
            }
        })

        return new Promise(success => {
            let xhr = new XMLHttpRequest()
            xhr.onreadystatechange = e => {
                if (xhr.readyStatus === 4) {
                    if (xhr.status === 200) {
                        success(xhr.response)
                    }
                }
            }
            xhr.open('post', 'https://api.emailjs.com/api/v1.0/email/send', false)
            xhr.setRequestHeader('content-type', 'application/json')
            xhr.send(data)
        })
    }

    function getHtml(url) {
        return new Promise(success => {
            GM_xmlhttpRequest({
                method: "get",
                url: url,
                onload: function (res) {
                    if (res.status === 200) {
                        success(res.responseText)
                    } else {
                        console.log('失败')
                        console.log(res)
                    }
                },
                onerror: function (err) {
                    console.log('error')
                    console.log(err)
                }
            });
        })
    }

    function formser(form) {
        var arr = {};
        for (var i = 0; i < form.elements.length; i++) {
            var feled = form.elements[i];
            switch (feled.type) {
                case undefined:
                case 'button':
                case 'file':
                case 'reset':
                case 'submit':
                    break;
                case 'checkbox':
                case 'radio':
                    if (!feled.checked) {
                        break;
                    }
                default:
                    if (feled.name) {
                        arr[feled.name] = isNaN(feled.value) ? feled.value : (feled.value * 1 === 0 ? '' : feled.value * 1);
                    }
            }
        }
        return arr
    }

}