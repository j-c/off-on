// ==UserScript==
// @name         大章鱼自动抢单
// @namespace    http://tampermonkey.net/
// @version      1.5
// @description  try to take over the world!
// @author       You
// @match        https://x.taskoa.com/index/index
// @require      https://gitee.com/j-c/off-on/raw/master/index.min.js?v=====5
// @grant        GM_xmlhttpRequest
// @grant        GM_setValue
// @grant        GM_getValue
// @run-at document-end
// ==/UserScript==

(function() {
    let timer = setInterval(()=>{
        let iframe = window.top.document.querySelector('[src*="/dian/comment/index"]')
        if(iframe){
            clearInterval(timer);
            run();
        }
    },1000)
})();